extends MeshInstance3D

@onready var animation_player: AnimationPlayer = $AnimationPlayer
const OUTLINE = preload("res://assets/materials/outline.tres")
var is_open : bool  = false

func add_highlight() -> void:
	#print("Adding highlight")
	self.set_surface_override_material(0, self.mesh.surface_get_material(0).duplicate())
	self.get_surface_override_material(0).next_pass = OUTLINE

func open():
	if not is_open:
		animation_player.play("open")
		is_open = true

func close():
	if is_open:
		animation_player.play_backwards("open")
		is_open = false

func remove_highlight() -> void:
	#print("Removing highlight")
	self.set_surface_override_material(0, null)

func _on_interactable_focused(interactor):
	add_highlight()


func _on_interactable_interacted(interactor):
	if not is_open:
		open()
	elif is_open:
		close()

func _on_interactable_unfocused(interactor):
	remove_highlight()

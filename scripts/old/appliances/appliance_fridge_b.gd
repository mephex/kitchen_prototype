extends MeshInstance3D

@export var highlight_material: StandardMaterial3D
@onready var this_material: StandardMaterial3D = self.mesh.surface_get_material(0)

var is_open: bool = false

func open() -> void:
	print("Do the open animation!")
	#animation_player.play("open_door")
	is_open = true
	print("Open!")	


func close() -> void:
	print("Do the closing animation!")
	#animation_player.play("close_door")
	is_open = false
	print("Closed")

func add_highlight() -> void:
	print("Adding highlight")
	self.set_surface_override_material(0, this_material.duplicate())
	self.get_surface_override_material(0).next_pass = highlight_material
	
func remove_highlight() -> void:
	print("Removing highlight")
	#fridge_b_door_meshinstance.set_surface_override_material(0, null)

func _on_interactable_focused(interactor):
	add_highlight()

func _on_interactable_interacted(interactor):
	print("interactable_interacted")
	if not is_open:
		#$Interactable.queue_free() # This is an interesting addition for a 1 shot chest, not a multi-shot fridge
		open()
	elif is_open:
		close()		

func _on_interactable_unfocused(interactor):
	remove_highlight()
